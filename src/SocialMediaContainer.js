import { _Params } from './Core'
import { SocialMediaButton } from './SocialMediaButton'

export class SocialMediaContainer extends Sprite {
  /**
   * A container to hold all social media buttons
   *
   * @param {*} x - The x position of the container
   * @param {*} y - The y position of the container
   */
  constructor (x, y) {
    super()
    this._listener = this.processButtonTouching.bind(this)
    document.addEventListener('mousemove', this._listener)
    this.createSocialButtons()
  }

  createSocialButtons () {
    let x = 0
    _Params.buttonsList.forEach(social => {
      if (!social) { return }
      const path = social.image.split('/')
      const data = {
        src: ImageManager.loadBitmap(`img/${path[0]}/`, path[1]),
        url: social.url
      }
      const button = new SocialMediaButton(data, x, 0)
      button.bitmap.addLoadListener(() => {
        button.setPosition(x, 0)
        this.addChild(button)
        x += _Params.settings.spacing + button.width
        this.resize()
      })
    })
  }

  processButtonTouching (event) {
    this.children.forEach(button => button.updateMousePosition(event))
  }

  setPosition () {
    /* eslint-disable no-eval */
    const x2 = eval(_Params.settings.x)
    const y2 = eval(_Params.settings.y)
    this.x = x2
    this.y = y2 - this.getBounds().height
    if (this.x + this.getBounds().width > Graphics.boxWidth) {
      this.x = x2 - this.getBounds().width
    }
  }

  resize () {
    this.calculateBounds()
    this.setPosition()
  }

  removeListener () {
    document.removeEventListener('mousemove', this._listener)
  }
}

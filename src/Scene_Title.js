import { SocialMediaContainer } from './SocialMediaContainer'

const Alias_SceneTitle_create = Scene_Title.prototype.create
Scene_Title.prototype.create = function () {
  Alias_SceneTitle_create.call(this)
  this.createSocialContainer()
}

Scene_Title.prototype.createSocialContainer = function () {
  this._socialMediaContainer = new SocialMediaContainer()
  this.addChild(this._socialMediaContainer)
}

const Alias_SceneTitle_terminate = Scene_Title.prototype.terminate
Scene_Title.prototype.terminate = function () {
  Alias_SceneTitle_terminate.call(this)
  this._socialMediaContainer.removeListener()
}

import { _Params } from './Core'

export class SocialMediaButton extends Sprite_Button {
  /**
   * A button for social media icons/images
   *
   * @param {*} src - The image source for the button
   * @param {*} x - The x position of the button
   * @param {*} y - The y position of the button
   */
  constructor (data, x, y) {
    super()
    super.initialize()
    this._data = data
    this.bitmap = data.src
    this.setClickHandler(this.openLink)
    this._colorMatrix = new PIXI.filters.ColorMatrixFilter()
    this.filters = [this._colorMatrix]
    this._isFiltered = false
    this._mousePos = {}
  }

  updateMousePosition (event) {
    this._mousePos.x = event.pageX
    this._mousePos.y = event.pageY
  }

  update () {
    super.update()
  }

  updateFrame (event) {
    const filter = _Params.onTouchFilter
    if (this.isButtonTouching() && !this._isFiltered) {
      this._colorMatrix[filter.type](filter.value1, filter.value2)
      this._isFiltered = true
      AudioManager.playSe(_Params.onTouchSound)
    } else if (this._isFiltered && !this.isButtonTouching()) {
      this._colorMatrix.reset()
      this._isFiltered = false
    }
  }

  isButtonTouching () {
    if (!this._mousePos.x || !this._mousePos.y) { return false }
    const x = this.canvasToLocalX(Graphics.pageToCanvasX(this._mousePos.x))
    const y = this.canvasToLocalY(Graphics.pageToCanvasY(this._mousePos.y))
    return x >= 0 && y >= 0 && x < this.width && y < this.height
  }

  setPosition (x, y) {
    this.x = x
    this.y = y
  }

  openLink () {
    if (Utils.isNwjs()) {
      try {
        AudioManager.playSe(_Params.settings.onClickSound)
        window.nw.Shell.openExternal(this._data.url)
      } catch (error) {
        console.log(error)
      }
    } else if (!Utils.isNwjs()) {
      AudioManager.playSe(_Params.settings.onClickSound)
      window.open(this._data.link)
    }
  }
}

/*:
 * @plugindesc v 1.0.0 Place social media buttons on your title screen
 * <X_SocialMediaButtons>
 * @author FeniXEngine Contributors (https://fenixenginemv.gitlab.io/)
 * @pluginname X_SocialMediaButtons
 * @modulename X_SocialMediaButtons
 * @external
 * @required
 *
 * @param settings
 * @text General Settings
 * @type struct<Settings>
 * @desc The position & spacing for the social media images.
 * @default {"x":"Graphics.boxWidth - 5","y":"Graphics.boxHeight - 5","spacing":"15","onClickSound":"{\"name\":\"Decision3\",\"volume\":\"65\",\"pitch\":\"100\",\"pan\":\"0.5\"}"}
 *
 * @param onTouchFilter
 * @text On Touch Filter
 * @type struct<ButtonFilter>
 * @desc The filter applied to Image when touched. [See Help] Does not work well with black images!
 * @default {"type":"hue","value1":"55","value2":"false"}
 *
 * @param onTouchSound
 * @text On Touch Sound
 * @type struct<SoundEffect>
 * @desc The sound played when button is touched
 * @default {"name":"Cursor3","volume":"75","pitch":"150","pan":"0.5"}
 *
 * @param buttonsList
 * @text Buttons List
 * @type struct<ButtonPreset>[]
 * @desc All the buttons to be drawn on screen.
 * @default ["{\"url\":\"http://twitter.com/\",\"pos\":\"1\",\"image\":\"pictures/twitter\"}","{\"url\":\"http://facebook.com/\",\"pos\":\"2\",\"image\":\"pictures/facebook\"}","{\"url\":\"http://youtube.com/\",\"pos\":\"3\",\"image\":\"pictures/youtube\"}","{\"url\":\"http:///plus.google.com/.com/\",\"pos\":\"5\",\"image\":\"pictures/google-plus\"}"]
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 MIT License -

 * Free for use in any RPG Maker MV game project, commerical or otherwise

 * Credit may go to FeniXEngine Contributors or FeniXEngine

 * Though not required, you may provide a link back to the original source code,
   repository or website.

 -------------------------------------------------------------------------------
 # INFORMATION

  It adds social media images to your title screen, allows for special effects
  like applying a filter and playing a sound effect when touching one.
  Upon click it will take the user to an external URL using the devices preferred
  application like the browser or twitter app.

--------------------------------------------------------------------------------
 # Instructions

 * Install the plugin in your projects js/plugins directory

 * Save your Social Media images in a sub directory within your img/ directory

 * Adjust the parameters to match your social media profiles.

 # Color Matrix Filter

  Here is some information on the filter being applied, it's called a color matrix
  and in layman's terms, it adjusts the images rgba values which can create some
  very cool looking effects. While all this may be a bit advanced to understand
  there is nothing stopping you from testing them all out to see which one you
  prefer, don't worry you won't break anything 😉.

  -----------------------------------------------
   Filters

  Type              Value1            Value2
  -----------------------------------------------
  hue              rotation            multiply
  greyscale        scale               multiply
  night            intensity           multiply
  brightness       b(0-1)              multiply
  predator         amount              multiply
  saturate         amount              multiply
  contrast         amount              multiply

  Value1 - The amount/intensity of the filter being applied.
  Value2 - Which is always multiply is a true or false value. (If true the color
         matrix is multiplied.)
*/

/* eslint-disable spaced-comment */

/*~struct~ButtonPreset:
  @param url
  @text Website URL
  @default http://
  @desc The x position of the button container.

  @param image
  @text Image
  @type file
  @dir img/
  @desc The image to use for this button.
*/
/*~struct~Settings:
  @param x
  @text x Position
  @default Graphics.boxWidth
  @desc The x position of the button container.

  @param y
  @text Y Position
  @default Graphics.boxHeight
  @desc The y position of the buttons container.

  @param spacing
  @text Spacing
  @default 15
  @type number
  @desc The amount of space between each button

  @param onClickSound
  @text On Click Sound
  @default {"name":"Decision3","volume":"65","pitch":"1","pan":"0.5"}
  @type struct<SoundEffect>
  @desc The sound effect to play when clicking a button
*/

/*~struct~ButtonFilter:
  @param type
  @text Filter Type
  @type select
  @option hue
  @option greyscale
  @option saturate
  @option night
  @option brightness
  @option predator
  @option contrast
  @default greyscale
  @desc The type of filter to apply to the button on touch

  @param value1
  @text Intensity
  @type number
  @default 1
  @desc The amount/intensity of the filter being applied.

  @param value2
  @text Value 2
  @default false
  @type bool
  @desc If true the matrix is multiplied.
*/
/*~struct~SoundEffect:
  @param name
  @text Name
  @type file
  @dir audio/se/
  @desc The name of the sound effect

  @param volume
  @text Volume
  @type number
  @min 1
  @max 100
  @Default 50

  @param pitch
  @text Pitch
  @type number
  @min 1
  @max 200
  @Default 100

  @param pan
  @text Pan
  @type number
  @default 0.5
  @decimals 0.1
  @min 0
  @max 1
*/

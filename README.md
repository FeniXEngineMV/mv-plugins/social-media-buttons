# Social Media Buttons

# What does this plugin do?

It adds social media images to your title screen, allows for special effects like
applying a filter and playing a sound effect when touching one. Upon click it
will take the user to an external URL using the devices preferred application
like the browser or twitter app.


# Terms Of Use

* All plugin under the FeniXEngineMV name are MIT License
* Free for use in any RPG Maker MV game project, commercial or otherwise
* Credit may go to FeniXEngine Contributors or FeniXEngine but is optional
* Though not required, you may provide a link back to the original source code, repository or website.
